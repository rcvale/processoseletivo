package caed.prova.relatoriomodel.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RelatorioCandidatosAprovados implements Serializable{
	
	private String titulo;
	private String dataRelatorio;
	private List<CandidatoReport> listCandidatos = new ArrayList<CandidatoReport>();
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getDataRelatorio() {
		return dataRelatorio;
	}
	public void setDataRelatorio(String dataRelatorio) {
		this.dataRelatorio = dataRelatorio;
	}
	public List<CandidatoReport> getListCandidatos() {
		return listCandidatos;
	}
	public void setListCandidatos(List<CandidatoReport> listCandidatos) {
		this.listCandidatos = listCandidatos;
	}

}
