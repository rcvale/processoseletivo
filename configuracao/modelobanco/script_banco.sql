CREATE DATABASE processo_seletivo
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'Portuguese_Brazil.1252'
    LC_CTYPE = 'Portuguese_Brazil.1252'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
	

-- Table: public.usuario

-- DROP TABLE public.usuario;

CREATE TABLE public.usuario
(
    cod_usuario serial NOT NULL,
    nome character varying(100) COLLATE pg_catalog."default" NOT NULL,
    login character varying(10) COLLATE pg_catalog."default" NOT NULL,
    senha character varying(10) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT usuario_pkey PRIMARY KEY (cod_usuario)
)

TABLESPACE pg_default;

ALTER TABLE public.usuario OWNER to postgres;




-- Table: public.status

-- DROP TABLE public.status;

CREATE TABLE public.status
(
    cod_status integer NOT NULL,
    nome character varying(20) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT status_pkey PRIMARY KEY (cod_status)
)

TABLESPACE pg_default;

ALTER TABLE public.status OWNER to postgres;




-- Table: public.candidato

-- DROP TABLE public.candidato;

CREATE TABLE public.candidato
(
    cod_candidato serial NOT NULL,
    nome character varying(100) COLLATE pg_catalog."default" NOT NULL,
    email character varying(100) COLLATE pg_catalog."default" NOT NULL,
    sexo character varying(1) COLLATE pg_catalog."default" NOT NULL,
    telefone character varying(11) COLLATE pg_catalog."default",
    endereco character varying(100) COLLATE pg_catalog."default",
    observacao character varying(1000) COLLATE pg_catalog."default",
    nota integer,
    cod_status integer NOT NULL,
    CONSTRAINT candidato_pkey PRIMARY KEY (cod_candidato),
    CONSTRAINT cod_status FOREIGN KEY (cod_status)
        REFERENCES public.status (cod_status) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)

TABLESPACE pg_default;

ALTER TABLE public.candidato OWNER to postgres;
	
	
	

INSERT INTO public.status(
	cod_status, nome)
	VALUES (1, 'EM AVALIAÇÃO');
	
INSERT INTO public.status(
	cod_status, nome)
	VALUES (2, 'APROVADO');
	
INSERT INTO public.status(
	cod_status, nome)
	VALUES (3, 'REPROVADO');


INSERT INTO public.usuario(
	nome, login, senha)
	VALUES ('ADMINISTRADOR', 'ADM', '123');
	

	