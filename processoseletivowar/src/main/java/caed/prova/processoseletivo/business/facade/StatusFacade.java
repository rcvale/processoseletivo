package caed.prova.processoseletivo.business.facade;

import java.util.List;

import caed.prova.processoseletivo.business.service.StatusService;
import caed.prova.processoseletivo.exception.FacadeException;
import caed.prova.processoseletivo.exception.ServiceException;
import caed.prova.processoseletivo.model.Status;

public class StatusFacade {

	private StatusService statusService;

	public StatusFacade() {
		statusService = new StatusService();
	}
	
	public List<Status> getStatusParaRelatorio() throws FacadeException {
		try {
			return statusService.getStatusParaRelatorio();
		} catch (ServiceException e) {
			throw new FacadeException(e.getMessage(), e);
		}
	}

}
