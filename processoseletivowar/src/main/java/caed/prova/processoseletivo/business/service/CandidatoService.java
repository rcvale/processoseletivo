package caed.prova.processoseletivo.business.service;

import java.util.List;

import caed.prova.processoseletivo.exception.DAOException;
import caed.prova.processoseletivo.exception.ServiceException;
import caed.prova.processoseletivo.integration.dao.CandidatoDAO;
import caed.prova.processoseletivo.integration.dao.util.ordenacao.OrderBy;
import caed.prova.processoseletivo.model.Candidato;
import caed.prova.processoseletivo.model.Status;

public class CandidatoService {
		
	public Candidato gravarCandidato(Candidato obj) throws ServiceException{		
		try {
			Candidato c = null;
			
			if(obj.getCodigo()!=null && obj.getCodigo().longValue() > 0) {
				c = new CandidatoDAO().update(obj);
			}
			else {
				obj.setStatus(new Status());
				obj.getStatus().setCodigo(Status.EM_AVALIACAO);
				c = new CandidatoDAO().insert(obj);
			}
			
			return c;
		}
		catch (DAOException e) {
			throw new ServiceException(e.getMessage(), e);
		}
	}

	public Candidato adicionarNota(Candidato canditato) throws ServiceException{
		try {
			if(canditato.getNota()==null)
				throw new ServiceException("A nota do candidato deve ser informada.");
			
			if(canditato.getNota().intValue() < 0 || canditato.getNota().intValue() > 10)
				throw new ServiceException("Nota inválida. A nota não deve ser de 0 a 10.");
			
			canditato.getStatus().setCodigo(canditato.getNota().intValue() >= 7 ? Status.APROVADO : Status.REPROVADO);
			
			return new CandidatoDAO().update(canditato);
		}
		catch (DAOException e) {
			throw new ServiceException(e.getMessage(), e);
		}
	}
	
	public void excluir(Long id) throws ServiceException{		
		try {
			new CandidatoDAO().delete(id);
		}
		catch (DAOException e) {
			throw new ServiceException(e.getMessage(), e);
		}
	}

	public List<Candidato> find(Candidato search, List<OrderBy> ordenacao) throws ServiceException{
		try {
			return new CandidatoDAO().find(search, ordenacao);
		}
		catch (DAOException e) {
			throw new ServiceException(e.getMessage(), e);
		}
	}
	
	public Candidato findById(Long id) throws ServiceException{
		try {
			return new CandidatoDAO().findById(id);
		}
		catch (DAOException e) {
			throw new ServiceException(e.getMessage(), e);
		}
	}

}
