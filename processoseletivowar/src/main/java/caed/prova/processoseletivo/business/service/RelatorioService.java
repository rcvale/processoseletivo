package caed.prova.processoseletivo.business.service;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import caed.prova.processoseletivo.exception.ServiceException;
import caed.prova.processoseletivo.integration.dao.util.ordenacao.Ordenacao;
import caed.prova.processoseletivo.integration.dao.util.ordenacao.OrderBy;
import caed.prova.processoseletivo.model.Candidato;
import caed.prova.processoseletivo.model.Status;
import caed.prova.relatoriomodel.model.CandidatoReport;
import caed.prova.relatoriomodel.model.RelatorioCandidatosAprovados;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;

public class RelatorioService {
	
	private static final String DIRETORIO = "C:";
	private static final String PATH_RELATORIOS = DIRETORIO + File.separator + "relatorios" + File.separator;
	
	public File gerarRelatorioCandidato(long codUsuario, long codStatus) throws ServiceException{
		try {
			String pathRelatorioGeradoUsuario = PATH_RELATORIOS + codUsuario + File.separator;
			if(!new File(pathRelatorioGeradoUsuario).isDirectory())
				new File(pathRelatorioGeradoUsuario).mkdirs();
			
			RelatorioCandidatosAprovados dadosRelatorio = new RelatorioCandidatosAprovados();
			dadosRelatorio.setTitulo("Lista de candidatos " + (codStatus == Status.APROVADO ? "aprovados" : "reprovados"));
			dadosRelatorio.setDataRelatorio(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));
			
			String relatorioPDF = pathRelatorioGeradoUsuario + "Candidatos" + (codStatus == Status.APROVADO ? "Aprovados" : "Reprovados") + ".pdf";
			

			Candidato search = new Candidato();
			search.setStatus(new Status());
			search.getStatus().setCodigo(codStatus);
			
			List<Candidato> listCandidatos = new CandidatoService().find(
					search, 
					Arrays.asList(new OrderBy("nota", Ordenacao.DESC), new OrderBy("nome", Ordenacao.ASC))
				);
			
			if (listCandidatos != null && listCandidatos.size() > 0) {
				List<CandidatoReport> listCanditatosReport = new ArrayList<>();
				
				for (Candidato candidato : listCandidatos) {
					CandidatoReport c = new CandidatoReport();
					c.setCodigo(candidato.getCodigo());
					c.setNome(candidato.getNome());
					c.setSexo(candidato.getSexo());
					c.setNota(candidato.getNota());
					listCanditatosReport.add(c);
				}
				
				dadosRelatorio.setListCandidatos(listCanditatosReport);
			}
			
			
			Map<String, Object> parametros = new HashMap<>();
			
			
			JasperPrint print = JasperFillManager.fillReport(
					PATH_RELATORIOS + "CandidatosPorStatus.jasper", 
					parametros, 
					new JRBeanCollectionDataSource(Arrays.asList(dadosRelatorio))
				);
			
			JRPdfExporter exporter = new JRPdfExporter();

			exporter.setExporterInput(new SimpleExporterInput(print));			
			exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(relatorioPDF));
			SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
			exporter.setConfiguration(configuration);

			exporter.exportReport();
			
			
			return new File(relatorioPDF);
			
		}catch (Exception e) {
			throw new ServiceException(e.getMessage(), e);
		}
		
	}

}
