package caed.prova.processoseletivo.business.service;

import java.util.List;

import caed.prova.processoseletivo.exception.DAOException;
import caed.prova.processoseletivo.exception.ServiceException;
import caed.prova.processoseletivo.integration.dao.UsuarioDAO;
import caed.prova.processoseletivo.model.Usuario;

public class UsuarioService {
		
	public Usuario getUsuarioLogin(String login, String senha) throws ServiceException{		
		try {
			Usuario search = new Usuario();
			search.setLogin(login.toUpperCase());
			search.setSenha(senha.toUpperCase());
			
			List<Usuario> list = new UsuarioDAO().find(search);
			if(list!=null && list.size()>0)
				return list.get(0);
			
			return null;
		}
		catch (DAOException e) {
			throw new ServiceException(e.getMessage(), e);
		}
	}

}
