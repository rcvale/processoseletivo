package caed.prova.processoseletivo.business.service;

import java.util.Arrays;
import java.util.List;

import caed.prova.processoseletivo.exception.DAOException;
import caed.prova.processoseletivo.exception.ServiceException;
import caed.prova.processoseletivo.integration.dao.StatusDAO;
import caed.prova.processoseletivo.integration.dao.util.ordenacao.Ordenacao;
import caed.prova.processoseletivo.integration.dao.util.ordenacao.OrderBy;
import caed.prova.processoseletivo.model.Status;

public class StatusService {
	
	public List<Status> getStatusParaRelatorio() throws ServiceException{
		try {
			List<Status> list = new StatusDAO().find(new Status(), Arrays.asList(new OrderBy("nome", Ordenacao.ASC)));			
			list.removeIf(s -> s.getCodigo().longValue() == Status.EM_AVALIACAO);			
			return list;
		}
		catch (DAOException e) {
			throw new ServiceException(e.getMessage(), e);
		}
	}

}
