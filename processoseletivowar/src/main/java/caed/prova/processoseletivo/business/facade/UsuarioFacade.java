package caed.prova.processoseletivo.business.facade;

import caed.prova.processoseletivo.business.service.UsuarioService;
import caed.prova.processoseletivo.exception.FacadeException;
import caed.prova.processoseletivo.exception.ServiceException;
import caed.prova.processoseletivo.model.Usuario;

public class UsuarioFacade {
	
	private UsuarioService usuarioService;
	
	public UsuarioFacade() {
		usuarioService = new UsuarioService();
	}
	
	public Usuario getUsuarioLogin(String login, String password) throws FacadeException{		
		try {
			return usuarioService.getUsuarioLogin(login, password);
		}catch(ServiceException e) {
			throw new FacadeException(e.getMessage(), e);
		}
	}

}
