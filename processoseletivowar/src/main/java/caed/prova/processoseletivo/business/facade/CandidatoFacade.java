package caed.prova.processoseletivo.business.facade;

import java.util.List;

import caed.prova.processoseletivo.business.service.CandidatoService;
import caed.prova.processoseletivo.exception.FacadeException;
import caed.prova.processoseletivo.exception.ServiceException;
import caed.prova.processoseletivo.integration.dao.util.ordenacao.OrderBy;
import caed.prova.processoseletivo.model.Candidato;

public class CandidatoFacade {

	private CandidatoService candidatoService;

	public CandidatoFacade() {
		candidatoService = new CandidatoService();
	}

	public Candidato gravarCandidato(Candidato obj) throws FacadeException {
		try {
			return candidatoService.gravarCandidato(obj);
		} catch (ServiceException e) {
			throw new FacadeException(e.getMessage(), e);
		}
	}

	public Candidato adicionarNota(Candidato canditato) throws FacadeException {
		try {
			return candidatoService.adicionarNota(canditato);
		} catch (ServiceException e) {
			throw new FacadeException(e.getMessage(), e);
		}
	}
	
	public void excluir(Long id) throws FacadeException {
		try {
			candidatoService.excluir(id);
		} catch (ServiceException e) {
			throw new FacadeException(e.getMessage(), e);
		}
	}
	
	public List<Candidato> find(Candidato search, List<OrderBy> ordenacao) throws FacadeException {
		try {
			return candidatoService.find(search, ordenacao);
		} catch (ServiceException e) {
			throw new FacadeException(e.getMessage(), e);
		}
	}
	
	public Candidato findById(Long id) throws FacadeException {
		try {
			return candidatoService.findById(id);
		} catch (ServiceException e) {
			throw new FacadeException(e.getMessage(), e);
		}
	}
	

}
