package caed.prova.processoseletivo.business.facade;

import java.io.File;

import caed.prova.processoseletivo.business.service.RelatorioService;
import caed.prova.processoseletivo.exception.FacadeException;
import caed.prova.processoseletivo.exception.ServiceException;
import caed.prova.processoseletivo.model.Candidato;

public class RelatorioFacade {

	private RelatorioService RelatorioService;

	public RelatorioFacade() {
		RelatorioService = new RelatorioService();
	}
	
	public File gerarRelatorioCandidato(Long codUsuario, Long codStatus) throws FacadeException {
		try {
			return RelatorioService.gerarRelatorioCandidato(codUsuario, codStatus);
		} catch (ServiceException e) {
			throw new FacadeException(e.getMessage(), e);
		}
	}

}
