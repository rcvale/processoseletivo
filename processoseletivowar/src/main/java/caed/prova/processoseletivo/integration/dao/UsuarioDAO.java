package caed.prova.processoseletivo.integration.dao;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;

import caed.prova.processoseletivo.exception.DAOException;
import caed.prova.processoseletivo.integration.dao.util.GenericDao;
import caed.prova.processoseletivo.model.Usuario;

public class UsuarioDAO extends GenericDao<Usuario> {

	@Override
	protected Class<Usuario> getType() {
		return Usuario.class;
	}
	
	@Override
	protected Predicate where(Usuario search, CriteriaBuilder cb, Root<Usuario> from) throws DAOException {
		Predicate p = null;

		if (search.getCodigo() != null && search.getCodigo() > 0) {
			Predicate p1 = cb.equal(from.get("codigo"), search.getCodigo());
			p = p == null ? p1 : cb.and(p, p1);
		}

		if (StringUtils.isNotBlank(search.getNome())) {
			Predicate p1 = cb.equal(from.get("nome"), search.getNome());
			p = p == null ? p1 : cb.and(p, p1);
		}

		if (StringUtils.isNotBlank(search.getLogin())) {
			Predicate p1 = cb.equal(from.get("login"), search.getLogin());
			p = p == null ? p1 : cb.and(p, p1);
		}

		if (StringUtils.isNotBlank(search.getSenha())) {
			Predicate p1 = cb.equal(from.get("senha"), search.getSenha());
			p = p == null ? p1 : cb.and(p, p1);
		}

		return p;
	}

	

}
