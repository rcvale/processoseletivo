package caed.prova.processoseletivo.integration.dao;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;

import caed.prova.processoseletivo.exception.DAOException;
import caed.prova.processoseletivo.integration.dao.util.GenericDao;
import caed.prova.processoseletivo.model.Candidato;

public class CandidatoDAO extends GenericDao<Candidato> {

	@Override
	protected Class<Candidato> getType() {
		return Candidato.class;
	}

	@Override
	protected Predicate where(Candidato search, CriteriaBuilder cb, Root<Candidato> from) throws DAOException {
		Predicate p = null;

		if (search.getCodigo() != null && search.getCodigo() > 0) {
			Predicate p1 = cb.equal(from.get("codigo"), search.getCodigo());
			p = p == null ? p1 : cb.and(p, p1);
		}

		if (StringUtils.isNotBlank(search.getNome())) {
			Predicate p1 = cb.equal(from.get("nome"), search.getNome());
			p = p == null ? p1 : cb.and(p, p1);
		}

		if (search.getStatus() != null) {
			if (search.getStatus().getCodigo() != null && search.getStatus().getCodigo() > 0) {
				Predicate p1 = cb.equal(from.get("status").get("codigo"), search.getStatus().getCodigo());
				p = p == null ? p1 : cb.and(p, p1);
			}
		}

		return p;
	}

}
