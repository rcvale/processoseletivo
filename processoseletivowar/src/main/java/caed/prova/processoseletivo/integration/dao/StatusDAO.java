package caed.prova.processoseletivo.integration.dao;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang.StringUtils;

import caed.prova.processoseletivo.exception.DAOException;
import caed.prova.processoseletivo.integration.dao.util.GenericDao;
import caed.prova.processoseletivo.model.Status;

public class StatusDAO extends GenericDao<Status> {

	@Override
	protected Class<Status> getType() {
		return Status.class;
	}
	
	@Override
	protected Predicate where(Status search, CriteriaBuilder cb, Root<Status> from) throws DAOException {
		Predicate p = null;

		if (search.getCodigo() != null && search.getCodigo() > 0) {
			Predicate p1 = cb.equal(from.get("codigo"), search.getCodigo());
			p = p == null ? p1 : cb.and(p, p1);
		}

		if (StringUtils.isNotBlank(search.getNome())) {
			Predicate p1 = cb.equal(from.get("nome"), search.getNome());
			p = p == null ? p1 : cb.and(p, p1);
		}

		return p;
	}

	

}
