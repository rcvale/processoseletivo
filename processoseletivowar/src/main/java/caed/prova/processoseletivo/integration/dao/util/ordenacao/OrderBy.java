package caed.prova.processoseletivo.integration.dao.util.ordenacao;

public class OrderBy {
	
	private String campo;
	private Ordenacao ordenacao;
	
	public OrderBy(String campo, Ordenacao ordenacao) {
		this.campo = campo;
		this.ordenacao = ordenacao;
	}
	
	public String getCampo() {
		return campo;
	}
	public Ordenacao getOrdenacao() {
		return ordenacao;
	}

}
