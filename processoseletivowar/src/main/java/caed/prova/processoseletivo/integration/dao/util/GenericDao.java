package caed.prova.processoseletivo.integration.dao.util;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import caed.prova.processoseletivo.exception.DAOException;
import caed.prova.processoseletivo.integration.dao.util.ordenacao.Ordenacao;
import caed.prova.processoseletivo.integration.dao.util.ordenacao.OrderBy;

public abstract class GenericDao<T> {

	private EntityManager entityManager;
	private EntityManagerFactory factory;


	protected abstract Class<T> getType();

	public T insert(T entity) throws DAOException {
		try {
			init();
			EntityTransaction t = entityManager.getTransaction();
			t.begin();
			entityManager.persist(entity);
			entityManager.flush();
			t.commit();
			return entity;
		} catch (Exception e) {
			throw new DAOException(e.getMessage(), e);
		}
		finally {
			close();
		}
	}

	public T update(T entity) throws DAOException {
		try {
			init();
			EntityTransaction t = entityManager.getTransaction();
			t.begin();
			entityManager.merge(entity);
			entityManager.flush();
			t.commit();
			return entity;
		} catch (Exception e) {
			throw new DAOException(e.getMessage(), e);
		}
		finally {
			close();
		}
	}

	public void delete(Object id) throws DAOException {
		try {			
			T entity = findById(id);
			
			init();
			EntityTransaction tx = entityManager.getTransaction();
			tx.begin();
			T mergedEntity = entityManager.merge(entity);
			entityManager.remove(mergedEntity);
			entityManager.flush();
			tx.commit();
		} catch (Exception e) {
			throw new DAOException(e.getMessage(), e);
		}
		finally {
			close();
		}
	}

	
	public List<T> find(T search) throws DAOException {
		return find(search, null);
	}
	
	public List<T> find(T search, List<OrderBy> ordenacoes) throws DAOException {
		try {
			init();
			CriteriaBuilder builder = entityManager.getCriteriaBuilder();
			
			Class<T> type = getType();
			CriteriaQuery<T> query = builder.createQuery(type);
			
			Root<T> from = query.from(type);			
			
			if(search!=null) {
				Predicate p = where(search, builder, from);
				if (p != null)
					query.where(p);
			}
			
			if(ordenacoes!=null && ordenacoes.size()>0) {
				List<Order> listOrder = new ArrayList<Order>();		
				for (OrderBy orderBy : ordenacoes) {					
					listOrder.add(orderBy.getOrdenacao().equals(Ordenacao.ASC) ? builder.asc(from.get(orderBy.getCampo())) : builder.desc(from.get(orderBy.getCampo())));					
				}
				if(listOrder.size()>0)
					query.orderBy(listOrder);
			}
			
			TypedQuery<T> createQuery = entityManager.createQuery(query);
			return createQuery.getResultList();

		} catch (Exception e) {
			throw new DAOException(e.getMessage(), e);
		}
		finally {
			close();
		}
	}

	public T findById(Object id) throws DAOException {
		try {
			init();
			return entityManager.find(getType(), id);
		} catch (Exception e) {
			throw new DAOException(e.getMessage(), e);
		}
		finally {
			close();
		}
	}

	private void init() {
		if(factory==null || !factory.isOpen())	
			factory = Persistence.createEntityManagerFactory("processoseletivo");
		if(entityManager==null || !entityManager.isOpen())	
			entityManager = factory.createEntityManager();
	}
	
	private void close() {
		if(entityManager!=null && entityManager.isOpen())
			entityManager.close();
		if(factory!=null && factory.isOpen())
			factory.close();
	}

	protected abstract Predicate where(T search, CriteriaBuilder cb, Root<T> from) throws DAOException;	

}