package caed.prova.processoseletivo.presentation.candidato;

import java.io.Serializable;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import caed.prova.processoseletivo.business.facade.CandidatoFacade;
import caed.prova.processoseletivo.model.Candidato;

@ManagedBean(name = "notaCandidatoBean")
@ViewScoped
public class NotaCandidatoBean implements Serializable {

	private Candidato canditato;

		
	@PostConstruct
    public void init() {			
		try {
			Map<String,Object> sessionMapObj = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			
			if(sessionMapObj.containsKey("codCandidato")) {
				Long codigo = (Long) sessionMapObj.get("codCandidato");
				sessionMapObj.remove("codCandidato");
				
				canditato = new CandidatoFacade().findById(codigo);
			}
			else {
				canditato = new Candidato();
			}
		}
		catch (Exception e) {
			System.out.println(e);//
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Erro ao abrir a tela de nota de candidato", "Ocorreu um erro inesperado ao abrir a tela de nota de candidato"));
		}		
    }
	
	
	public String adicionarNota() {
		try {
			new CandidatoFacade().adicionarNota(canditato);

		} catch (Exception e) {
			System.out.println(e);//
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Erro ao gravar candidato", e.getMessage()));
			
			return null;
		}		
		
		return "/candidato/lista.xhtml?faces-redirect=true";
	}
	
	public String voltar() {		
		return "/candidato/lista.xhtml?faces-redirect=true";
	}
	

	
	

	public Candidato getCanditato() {
		return canditato;
	}

	public void setCanditato(Candidato canditato) {
		this.canditato = canditato;
	}
	
	
}
