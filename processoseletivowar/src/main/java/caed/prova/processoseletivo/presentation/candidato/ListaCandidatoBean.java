package caed.prova.processoseletivo.presentation.candidato;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import caed.prova.processoseletivo.business.facade.CandidatoFacade;
import caed.prova.processoseletivo.integration.dao.util.ordenacao.Ordenacao;
import caed.prova.processoseletivo.integration.dao.util.ordenacao.OrderBy;
import caed.prova.processoseletivo.model.Candidato;
import net.sf.jasperreports.crosstabs.fill.calculation.BucketValueOrderDecorator.OrderPosition;

@ManagedBean(name = "listaCandidatoBean")
@ViewScoped
public class ListaCandidatoBean implements Serializable {

	private List<Candidato> lista = new ArrayList<Candidato>();

	@PostConstruct
	public void init() {
		
	}

	

	public String novo() {
		Map<String,Object> sessionMapObj = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();		
		if(sessionMapObj.containsKey("codCandidato")) {
			sessionMapObj.remove("codCandidato");
		}
		return "/candidato/cadastro.xhtml";
	}
	
	public String editar(Long codCandidato) {		
		Map<String,Object> sessionMapObj = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		sessionMapObj.put("codCandidato", codCandidato);		
		return "/candidato/cadastro.xhtml";
	}
	
	public String adicionarNota(Long codCandidato) {		
		Map<String,Object> sessionMapObj = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
		sessionMapObj.put("codCandidato", codCandidato);		
		return "/candidato/nota.xhtml";
	}
	
	public String excluir(Long codCandidato) {		
		try {
			new CandidatoFacade().excluir(codCandidato);			
			
		} catch (Exception e) {
			System.out.println(e);//
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Erro ao obter lista de candidatos", "Ocorreu um erro inesperado ao obter os candidatos cadastrados"));
		}
		return null;
	}
	
	
	public List<Candidato> getLista() {
		try {
			lista = new CandidatoFacade().find(
					new Candidato(), 
					Arrays.asList(new OrderBy("nome", Ordenacao.ASC))
				);
			
		} catch (Exception e) {
			System.out.println(e);//
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Erro ao obter lista de candidatos", "Ocorreu um erro inesperado ao obter os candidatos cadastrados"));
		}
		return lista;
	}

	public void setLista(List<Candidato> lista) {
		this.lista = lista;
	}

}
