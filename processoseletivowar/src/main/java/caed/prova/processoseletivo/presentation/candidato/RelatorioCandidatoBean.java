package caed.prova.processoseletivo.presentation.candidato;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpSession;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import caed.prova.processoseletivo.business.facade.RelatorioFacade;
import caed.prova.processoseletivo.business.facade.StatusFacade;
import caed.prova.processoseletivo.model.Candidato;
import caed.prova.processoseletivo.model.Status;
import caed.prova.processoseletivo.model.Usuario;

@ManagedBean(name = "relatorioCandidatoBean")
public class RelatorioCandidatoBean implements Serializable {

	private List<SelectItem> listStatus = new ArrayList<SelectItem>();
	private String codStatus;

	@PostConstruct
	public void init() {
		try {

			listStatus = new StatusFacade().getStatusParaRelatorio().stream()
					.map(s -> new SelectItem(s.getCodigo(), s.getNome())).collect(Collectors.toList());

			listStatus.add(0, new SelectItem("", "::selecione::"));

		} catch (Exception e) {
			System.out.println(e);//
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro ao abrir a tela de relatório de candidato",
							"Ocorreu um erro inesperado abrir a tela de relatório de candidato"));
		}
	}

	private StreamedContent file;

	public void setFile(StreamedContent file) {
		this.file = file;
	}

	public StreamedContent getFile() {
		try {			
			File relatorio = getRelatorio();		
			InputStream stream = new FileInputStream(relatorio);
			file = new DefaultStreamedContent(stream, "application/pdf", relatorio.getName());

			return file;
			
		}catch(Exception e) {
			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_ERROR, "Erro ao gerar relatório de candidato",
							"Ocorreu um erro inesperado ao gerar relatório de candidato: " + e.getMessage()));
			return null;
		}
		
	}

	private File getRelatorio() throws Exception {
		HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		Usuario usuario = (Usuario) session.getAttribute("usuariologado");

		File relatorio = new RelatorioFacade().gerarRelatorioCandidato(usuario.getCodigo(), new Long(codStatus));

		return relatorio;
	}
	

	public String voltar() {
		return "/candidato/lista.xhtml?faces-redirect=true";
	}

	public List<SelectItem> getListStatus() {
		return listStatus;
	}

	public void setListStatus(List<SelectItem> listStatus) {
		this.listStatus = listStatus;
	}

	public String getCodStatus() {
		return codStatus;
	}

	public void setCodStatus(String codStatus) {
		this.codStatus = codStatus;
	}

}
