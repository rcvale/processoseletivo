package caed.prova.processoseletivo.presentation.candidato;

import java.io.Serializable;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import caed.prova.processoseletivo.business.facade.CandidatoFacade;
import caed.prova.processoseletivo.model.Candidato;

@ManagedBean(name = "cadastroCandidatoBean")
@ViewScoped
public class CadastroCandidatoBean implements Serializable {

	private Candidato canditato;

		
	@PostConstruct
    public void init() {			
		try {
			Map<String,Object> sessionMapObj = FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
			
			if(sessionMapObj.containsKey("codCandidato")) {
				Long codigo = (Long) sessionMapObj.get("codCandidato");
				sessionMapObj.remove("codCandidato");
				
				canditato = new CandidatoFacade().findById(codigo);
			}
			else {
				canditato = new Candidato();
			}
		}
		catch (Exception e) {
			System.out.println(e);//
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Erro ao abrir a tela de cadastro", "Ocorreu um erro inesperado ao abrir a tela de cadastro de candidato"));
		}		
    }
	
	
	public String gravar() {
		try {
			new CandidatoFacade().gravarCandidato(canditato);

		} catch (Exception e) {
			System.out.println(e);//
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Erro ao gravar candidato", "Ocorreu um erro inesperado inesperado ao gravar o candidato"));
			
			return null;
		}		
		
		return "/candidato/lista.xhtml?faces-redirect=true";
	}
	
	public String voltar() {		
		return "/candidato/lista.xhtml?faces-redirect=true";
	}
	

	
	

	public Candidato getCanditato() {
		return canditato;
	}

	public void setCanditato(Candidato canditato) {
		this.canditato = canditato;
	}
	
	
}
