package caed.prova.processoseletivo.presentation.login;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import caed.prova.processoseletivo.business.facade.UsuarioFacade;
import caed.prova.processoseletivo.model.Usuario;

@ManagedBean(name = "loginBean")
@SessionScoped
public class LoginBean implements Serializable {

	private String login;
	private String senha;

	@PostConstruct
	public void init() {

	}

	public String efetuarLogin() {
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			ExternalContext externalContext = facesContext.getExternalContext();

			Usuario usuario = new UsuarioFacade().getUsuarioLogin(login, senha);

			if (usuario != null) {
				HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
				session.setAttribute("usuariologado", usuario);
				HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
				externalContext.redirect(request.getContextPath() + "/home.xhtml");
//				return "home";
			} else {
				FacesContext.getCurrentInstance().addMessage(null,
						new FacesMessage("Erro ao logar", "Usuário foi não encontrado"));
			}

		} catch (Exception e) {
			System.out.println(e);//
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Erro ao logar", "Ocorreu um erro inesperado ao efetuar o login"));
		}

		return null;
	}

	public String logout() {
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			ExternalContext externalContext = facesContext.getExternalContext();
			HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
			HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
			session.invalidate();
			externalContext.redirect(request.getContextPath() + "/home.xhtml");
			
		} catch (Exception e) {
			System.out.println(e);//
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Erro ao logar", "Ocorreu um erro inesperado ao efetuar o login"));
		}
		return null;
	}
	

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}
}
