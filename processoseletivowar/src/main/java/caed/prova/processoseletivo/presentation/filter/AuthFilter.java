package caed.prova.processoseletivo.presentation.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import caed.prova.processoseletivo.model.Usuario;

@WebFilter(filterName = "AuthFilter", urlPatterns = { "*.xhtml" })
public class AuthFilter implements Filter {

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		try {

			// check whether session variable is set
			HttpServletRequest req = (HttpServletRequest) request;
			HttpServletResponse res = (HttpServletResponse) response;
			HttpSession ses = req.getSession(false);
			// allow user to proccede if url is login.xhtml or user logged in or user is
			// accessing any page in //public folder
			String reqURI = req.getRequestURI();
			if (reqURI.indexOf("/authenticate") > 0) {
				chain.doFilter(request, response);
			} else {
				HttpSession session = ses;
				if ((reqURI.equals(req.getContextPath() + "/") || reqURI.indexOf("/login.xhtml") >= 0) && ses != null
						&& ses.getAttribute("usuariologado") != null) {
					String start = (String) ses.getAttribute("START");
					if (StringUtils.isNotBlank(start)) {
						res.sendRedirect(start);
					} else {

						if (session != null) {
							Usuario usuario = (Usuario) session.getAttribute("usuariologado");
							res.sendRedirect(req.getContextPath() + "/home.xhtml");
						} else {
							res.sendRedirect(req.getContextPath() + "/home.xhtml");
						}

					}
				} else if (reqURI.indexOf("/login.xhtml") >= 0
						|| (ses != null && ses.getAttribute("usuariologado") != null)
						|| reqURI.indexOf("/public/") >= 0 || reqURI.contains("javax.faces.resource")) {

					if (session != null) {
						Usuario usuario = (Usuario) session.getAttribute("usuariologado");
						chain.doFilter(request, response);
					} else {
						chain.doFilter(request, response);
					}

				} else { // user didn't log in but asking for a page that is not allowed so take user to
							// login page
					if (!reqURI.contains("javax.faces.resource") && reqURI.endsWith(".xhtml")) {
						if (ses == null) {
							ses = req.getSession(true);
							ses.setMaxInactiveInterval(120);
						}
						ses.setAttribute("START", reqURI);
					}

					res.sendRedirect(req.getContextPath() + "/login.xhtml"); // Anonymous user. Redirect to login page
				}
			}
		} catch (Throwable t) {
			System.err.println(t);
		}

	}

}
