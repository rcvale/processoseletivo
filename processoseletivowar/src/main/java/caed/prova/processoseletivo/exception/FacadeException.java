package caed.prova.processoseletivo.exception;

public class FacadeException extends Exception {

	public FacadeException() {
	}

	public FacadeException(String message) {
		super(message);
	}

	public FacadeException(Throwable cause) {
		super(cause);
	}

	public FacadeException(String message, Throwable cause) {
		super(message, cause);
	}

}
