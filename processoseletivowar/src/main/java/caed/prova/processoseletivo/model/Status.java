package caed.prova.processoseletivo.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "status")
public class Status implements Serializable {

	public static final long EM_AVALIACAO = 1;
	public static final long APROVADO = 2;
	public static final long REPROVADO = 3;
	
	@Id
	@Column(name = "cod_status")
	private Long codigo;

	@Column(name = "nome")
	private String nome;

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
