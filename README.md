# Processo seletivo

Projeto feito com Java 8, Primefaces 7 para ser executado no Tomcat 9 e Postgres 12.1 para o banco de dados. Para o relatório foi utilizado Jaspersoft Studio 6.12.2, pois agora é o client oficial para relatórios e não mais o ireport

O projeto consiste de um crud para cadastrar candidatos e inserir as notas. Ao informar a nota o candidato é marcado como aprovado ou reprovado. Para ser aprovado é necessário ter nota maior ou igual a 7.

Para o relatório existe uma tela para gerar o relatório em que é possível selecionar o status aprovado ou reprovado. O relatório é gerado com os candidados de acordo com o filtro.



## Configuração
Na pasta "configuracao" existe uma pasta contendo a lib do jdbc do postgres e sql para a criação das tabelas do banco.

Colocar o jar do jdbc na pasta lib do tomcat.

Na pasta conf do tomcat configurar o arquivo context.xml adicionando a conexão com o banco de dados como no exemplo abaixo mudando somente o necessário.

Não mudar o nome do resource

```
<Resource name="jdbc/PROCESSO_SELETIVO" auth="Container"
		type="javax.sql.DataSource" driverClassName="org.postgresql.Driver"
		url="jdbc:postgresql://127.0.0.1:5432/processo_seletivo"
		username="postgres" password="123456" maxTotal="20" maxIdle="10"
		maxWaitMillis="-1" />
```

Criar o banco de dados no postgre conforme o arquivo script_banco.sql.

Ainda na pasta de configuracao existe uma pasta chamada "relatorios". Colocar essa pasta na raiz do "C:", caso seja windows. Para outro sistema operacional colocar a pasta "relatorios" na pasta raiz e modificar no arquivo RelatorioService a constante "DIRETORIO" com o valor correto. É necessário ter permissão de leitura e escrita no diretório.



## Acesso
Url para abrir o projeto: http://localhost:8080/processoseletivowar

Para logar no sistema basta informar o login ADM e senha 123. Esses dados são de um registro da tabela "usuario" contendo esses dados.
